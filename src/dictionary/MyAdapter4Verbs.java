package dictionary;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.phraseverbs.R;

public class MyAdapter4Verbs extends ArrayAdapter<String> {

	Context context;
	ArrayList<String> values_orig, values_trans; 
    LayoutInflater inflater;
    TextView original, translate;
    RelativeLayout rl;
    static int current_position= -1;
    
	public MyAdapter4Verbs(Context con, ArrayList<String> list_orig, ArrayList<String> list_trans, int pos)
	{
		super(con, R.layout.dict_verbs_lv, list_orig);
		this.context = con;
		this.values_orig = list_orig;	
		this.values_trans = list_trans;	
		current_position = pos;
		inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
    {  	
    View row = inflater.inflate(R.layout.dict_verbs_lv, null);	
	original = (TextView)row.findViewById(R.id.dict_row_verb_tv);	
	translate = (TextView)row.findViewById(R.id.dict_row_verb_trans_tv);	
	rl = (RelativeLayout)row.findViewById(R.id.dict_verb_rl);	
	original.setText(values_orig.get(position));
	translate.setText(values_trans.get(position));
	if (position == current_position)
    { 
    {((TextView)row.findViewById(R.id.dict_row_verb_tv)).setTextColor(Color.BLACK);
	((TextView)row.findViewById(R.id.dict_row_verb_trans_tv)).setTextColor(Color.BLACK);
	((RelativeLayout)row.findViewById(R.id.dict_verb_rl)).setBackgroundResource(R.drawable.dict_verb_style_2);}
    }	return row;	
    	
	}




	



}
