package dictionary;

import java.util.ArrayList;

import com.example.phraseverbs.R;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MyAdapter4Proposition extends ArrayAdapter<String> {

	
	Context context;
	ArrayList<String> values; 
    LayoutInflater inflater;
    TextView original;
    View row;
    RelativeLayout rl;
    static int current_position= -1;
    
	public MyAdapter4Proposition(Context con, ArrayList<String> list)
	{
		super(con, R.layout.dict_propos_lv, list);
		this.context = con;
		this.values = list;	
		inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{    row = inflater.inflate(R.layout.dict_propos_lv, null);		   
		 original = (TextView)row.findViewById(R.id.dict_row_propos_tv);	
		 rl = (RelativeLayout)row.findViewById(R.id.dict_propos_rl);
		 original.setText(values.get(position));
		 if (position == current_position)
		    {  {((TextView)row.findViewById(R.id.dict_row_propos_tv)).setTextColor(Color.BLACK);
			((RelativeLayout)row.findViewById(R.id.dict_propos_rl)).setBackgroundResource(R.drawable.dict_verb_style_2);}
		    }
    	 return row;	
	}
}
