package dictionary;

import java.util.ArrayList;
import java.util.Locale;

import my.engapps.PhraseVerbsInUse.DBHelper;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.phraseverbs.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class Dictionary extends Activity   {


	ListView lv_verbs, lv_propos;
	TextView tv_orig, tv_trans;
	View verb_title, propos_title;
	SQLiteDatabase db;
	Cursor curVerbs, curPropos;
	View image_example, image_voice;
	Context context;
	Animation anim_example, anim_voice;
	TextToSpeech tts;
	boolean isVoice = false;
	boolean isHideProposLV = true;
	String temp_example_orig, temp_example_trans, temp_source;
	
	/////////////////////////////////////////////////////////////////////////////////////////
	//������������� ��� ����������� � �� ����� ����������� � ���������
	@Override
	protected void onResume() {
		super.onResume();
		db = new DBHelper(this).openDataBase();	
	}
	
/////////////////////////////////////////////////////////////////////////////////////	
	//��������� ����������� � �� � ��������� ���������� ������
	@Override 
	protected void onPause()
	{   db.close();
	    tts.stop();
	    tts.shutdown();
		super.onPause();
	}

	///////////////////////////////////////////////////////////////////////////
	//�������� ����, ��������� ������
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		ActionBar ab = this.getActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.options_menu, menu);
	    
	    SearchManager searchManager =
	            (SearchManager) getSystemService(Context.SEARCH_SERVICE);
	     SearchView searchView =
	             (SearchView) menu.findItem(R.id.search).getActionView();
	     searchView.setSearchableInfo(
	             searchManager.getSearchableInfo(getComponentName()));
	     
	     searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			
			@Override
			public boolean onQueryTextSubmit(String query) {
				return true;
			}
			
			@Override
			public boolean onQueryTextChange(String newText) {
				lv_propos.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 0));
				 propos_title.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 0));
				getVerbsCursor(newText);
				isHideProposLV = true;
				lv_verbs.setAdapter(new MyAdapter4Verbs(context, getDates(curVerbs, 1), getDates(curVerbs, 2), -1));	
				return false;
			}
		});
	     
		return true;
	}


///////////////////////////////////////////////////////////////////////////////
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home)
		{this.finish();}
		return super.onOptionsItemSelected(item);
	}
	

//////////////////////////////////////////////////////////////////////////////
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dictionary);
		context = this;
		db = new DBHelper(this).openDataBase();
		
		initSpeech(); 

	    getVerbsCursor("");
	    
	    AdView adView = (AdView)this.findViewById(R.id.adView);
	    AdRequest adRequest = new AdRequest.Builder().build();
	    adView.loadAd(adRequest);
			
	    tv_orig = (TextView)this.findViewById(R.id.dict_complete_orig);
	    tv_trans = (TextView)this.findViewById(R.id.dict_complete_trans);
	    
	    verb_title = this.findViewById(R.id.dict_title_lv_verb);
	    verb_title.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 5));
	    propos_title = this.findViewById(R.id.dict_title_lv_propos);
	    propos_title.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 0));
	    
	    
		lv_verbs = (ListView)this.findViewById(R.id.dictionary_verbs_lv);
		lv_verbs.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 5));
		lv_verbs.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int pos, 
					long arg3) {
				//���� ������� �������� �� ��������� �������
				if (pos != MyAdapter4Verbs.current_position)
				{   //��������� ������� ������� �������
				    MyAdapter4Verbs.current_position = (short)pos;
				    int fvp = lv_verbs.getFirstVisiblePosition();
				    //���������� ������� �������
				    MyAdapter4Proposition.current_position = -1;
				    //��������� ������� �������� ��� ���������� ���������
				    lv_verbs.setAdapter(new MyAdapter4Verbs(context, getDates(curVerbs, 1), getDates(curVerbs, 2), pos));
				    //��� ���������/����������� �������� �������
				    lv_verbs.setSelection(fvp);
				    if (isHideProposLV)
				        {lv_propos.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 3));
				         propos_title.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 3));
				         isHideProposLV = false;}				
				//��������� ������ ���������
				curPropos = getProposCursorForVerb(getIdVerbByText(v));
				//��������� ������� ���������
				lv_propos.setAdapter(new MyAdapter4Proposition(context, getDates(curPropos, 0)));}							
			}

		});

			
		lv_propos = (ListView)this.findViewById(R.id.dictionary_propos_lv);
		lv_propos.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 0));
		lv_propos.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int pos,
					long arg3) {				
				if (pos != MyAdapter4Proposition.current_position)
				{   MyAdapter4Proposition.current_position = pos;			
				    getResult();
				    lv_propos.setAdapter(new MyAdapter4Proposition(context, getDates(curPropos, 0)));
				    //������������ ������� �����
			      	if (image_example.getVisibility() == View.INVISIBLE)
			          	{image_example.setVisibility(View.VISIBLE);
				         image_example.setAnimation(anim_example);
			        	if (isVoice)
				            {image_voice.setVisibility(View.VISIBLE);
				             image_voice.setAnimation(anim_voice);
				        } 
			        }
				}
			}
		});
		
		image_example = this.findViewById(R.id.dict_example_image);
		image_example.setVisibility(View.INVISIBLE);
		image_example.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					AlertDialog.Builder adb = new AlertDialog.Builder(context);
					adb.setTitle("������ �������������");
					curPropos.moveToPosition(MyAdapter4Proposition.current_position);
					LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					View dialogview = inflater.inflate(R.layout.dict_alertdialog_example, null);
					   ((TextView)dialogview.findViewById(R.id.dict_ad_exampletext_orig_tv)).setText(temp_example_orig);
					   ((TextView)dialogview.findViewById(R.id.dict_ad_exampletext_trans_tv)).setText(temp_example_trans);
					   ((TextView)dialogview.findViewById(R.id.dict_ad_examplesource_tv)).setText(temp_source);
					adb.setView(dialogview);
					adb.setPositiveButton("��", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
				 			
						} 
					});
					AlertDialog ad = adb.create();
					ad.show(); 		
			}
		}); 
		
		image_voice = this.findViewById(R.id.dict_voice_image);
		image_voice.setVisibility(View.INVISIBLE);
		image_voice.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				tts.speak(tv_orig.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
		
			}
		});
		
		lv_verbs.setAdapter(new MyAdapter4Verbs(this.getApplicationContext(), getDates(curVerbs, 1), getDates(curVerbs, 2), -1));	
	    anim_example = AnimationUtils.loadAnimation(this, R.anim.dict_exampleimage_anim);
	    anim_voice = AnimationUtils.loadAnimation(this, R.anim.dict_voiceimage_anim);
	    
	    
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//���������� ������� ��������, �������� � ��������� ����� ��� ���������� ������������� � ������
    void getVerbsCursor(String s)
    {
    	this.curVerbs = db.rawQuery("select * from "+DBHelper.V+" where ORIGINAL like ?"+" order by "+"ORIGINAL", new String[] {s+"%"});
    }
	
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //������������� ���������� ������
    private void initSpeech() {
	tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {		
		@Override
		public void onInit(int status) {
			
			if(status == TextToSpeech.SUCCESS)
			{
				int result = tts.setLanguage(new Locale("en_US"));
				if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) 
				   {}
				else {isVoice = true;}
			}
		}
	});
	
}

///////////////////////////////////////////////////////////////////////////////////////////////	
/**
 * ������� ������ �� ������� � ������ ��� �������� ��������
 * @param i ����� ������� �� �������
 * @param cur ������ 
 * 
 */
	private ArrayList<String> getDates(Cursor cur, int i) {
		ArrayList<String> temp = new ArrayList<String> ();
		cur.moveToFirst();
		while (!cur.isAfterLast())
		{   
			temp.add(cur.getString(i));
			cur.moveToNext();
		}
		return temp;
	}	
	


	public Cursor getProposCursorForVerb(int pos)
	{
		Cursor temp = null;
		curVerbs.moveToPosition(pos);
	    temp = db.rawQuery("select " +
	    		"p.ORIGINAL, " +           //0     
	    		"v2p.translate, " +        //1
	    		"v2p.example_orig, " +     //2   
	    		"v2p.example_trans , " +   //3
	    		"v2p.sour�e " +            //4
	    		"from "+DBHelper.P+" as p inner join "+DBHelper.V2P+" as v2p on p._id = v2p.id_proposition where v2p.id_verb = ?", new String[] {""+pos});
		return temp;
	}
	
	/**
	 * ��������� ������ � ������ �����
	 * @param posVerb
	 * @param posPropos
	 */
	public void getResult()
	{   curVerbs.moveToPosition(MyAdapter4Verbs.current_position);
	    curPropos.moveToPosition(MyAdapter4Proposition.current_position);
		tv_orig.setText(""+curVerbs.getString(1)+" "+curPropos.getString(0));
		tv_trans.setText(curPropos.getString(1));
		temp_example_orig = curPropos.getString(2);
		temp_example_trans = curPropos.getString(3);
		temp_source = curPropos.getString(4);
	}
	
	//����� ���������� ID �������, ������ ��-�� ���������� 
	private int getIdVerbByText(View v) {
		String s = ((TextView)v.findViewById(R.id.dict_row_verb_tv)).getText().toString();
		Cursor cur = db.rawQuery("select * from "+DBHelper.V+" where ORIGINAL = ?", new String[] {s});
		cur.moveToFirst();	
		return cur.getInt(0);
	}
	
}
