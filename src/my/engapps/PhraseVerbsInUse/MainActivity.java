package my.engapps.PhraseVerbsInUse;

import information.Information;
import studying.Studying;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import checking.Checking;

import com.example.phraseverbs.R;

import dictionary.Dictionary;

public class MainActivity extends Activity implements OnClickListener{

	ImageView studying_iv, dictionary_iv, checking_iv, info_iv;
	View l1, l2, l3, l4;
	DBHelper db;
	int w, h; 
	int dist;
	int board;
	int bottom;
	
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);	
		w = metrics.widthPixels;
		h = metrics.heightPixels;
		dist = w*35/100;
		board = (int)(w*0.1); 
		bottom = (int)(w*0.3);
		
		studying_iv = (ImageView)this.findViewById(R.id.main_study_image);
		dictionary_iv = (ImageView)this.findViewById(R.id.main_dict_image);	
		checking_iv = (ImageView)this.findViewById(R.id.main_check_image);
		info_iv = (ImageView)this.findViewById(R.id.main_info_image);
				
	    l1 = this.findViewById(R.id.main_study_ll);
	    l2 = this.findViewById(R.id.main_check_ll);
	    l3 = this.findViewById(R.id.main_dict_ll);
	    l4 = this.findViewById(R.id.main_info_ll);
		
		correctSizeImage();

		studying_iv.setOnClickListener(this);
		dictionary_iv.setOnClickListener(this);
		checking_iv.setOnClickListener(this);
		info_iv.setOnClickListener(this);
		


	}
	
	@Override
	  public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.options_menu_main, menu);
	    return true;
	  } 
	
	@Override
	  public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    case R.id.close:
	      {
	    	  System.exit(0);
	      }
	    }
	    return true;
	  } 

	@SuppressLint("NewApi")
	private void correctSizeImage() {
		Bitmap b1 = BitmapFactory.decodeResource(getResources(), R.drawable.main_studying_image);
		Bitmap b2 = BitmapFactory.decodeResource(getResources(), R.drawable.main_checking_image);
		Bitmap b3 = BitmapFactory.decodeResource(getResources(), R.drawable.main_dictionary_image);
		Bitmap b4 = BitmapFactory.decodeResource(getResources(), R.drawable.main_info_image);
		
		studying_iv.setImageBitmap(Bitmap.createScaledBitmap(b1, dist, dist, false));		
		checking_iv.setImageBitmap(Bitmap.createScaledBitmap(b2, dist, dist, false));
		dictionary_iv.setImageBitmap(Bitmap.createScaledBitmap(b3, dist, dist, false));
		info_iv.setImageBitmap(Bitmap.createScaledBitmap(b4, dist, dist, false));
		
		if (Build.VERSION.SDK_INT < 11)
		{			
			RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			params1.leftMargin = board;
			params1.topMargin = h-(bottom+dist+board+dist);
			l1.setLayoutParams(params1);
			
			RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			params2.leftMargin = board+dist+board;
			params2.topMargin = h-(bottom+dist+board+dist);
			l2.setLayoutParams(params2);
			
			RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			params3.leftMargin = board;
			params3.topMargin = h-bottom-dist;
			l3.setLayoutParams(params3);
			
			RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			params4.leftMargin = board+dist+board;
			params4.topMargin = h-(bottom+dist); 
			l4.setLayoutParams(params4);
			
			
		}
		else 
		{l1.setX(board);  l1.setY(h-(bottom+dist+board+dist));
		l2.setX(board+dist+board); l2.setY(h-(bottom+dist+board+dist));
		l3.setX(board);  l3.setY(h-bottom-dist);
		l4.setX(board+dist+board); l4.setY(h-(bottom+dist));}
	}


	@Override
	public void onClick(View v) {
		Intent intent = null;
		switch (v.getId())
		{
		case R.id.main_study_image:
		   {
			   intent = new Intent(this, Studying.class);
			   break;
		   }
		case R.id.main_dict_image:
		   {
			   intent = new Intent(this, Dictionary.class);
			   break;
		   }
		   
		case R.id.main_check_image:
    		{   intent = new Intent(this, Checking.class);
				break;
			}
			
		case R.id.main_info_image:
		    {  intent = new Intent(this, Information.class);		 
		    	break;	   
	   	   }
		}
		
		this.startActivity(intent);

		
	}



}
