package my.engapps.PhraseVerbsInUse;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
	
	Context context;
	String path2DB = "//data//data//com.example.phraseverbs//databases//";
	static String DBName = "new.sqlite3.db";
	String path = path2DB+DBName;
	SQLiteDatabase db;
	static public final String V = "verbs", P = "propositions", V2P = "verb_2_proposition";
	
	public DBHelper(Context context)
	{super(context, DBName, null, 1);
    this.context = context;   
    openDataBase();
	}
	
	
	public SQLiteDatabase openDataBase()
	{  	if (db == null) {
			createDataBase();
			db = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.OPEN_READWRITE);
		}
		return db;
	}	
	
	public void createDataBase()
	{  	boolean existDB = checkDataBase();
		if (!existDB) {
			this.getReadableDatabase();
			try {
				this.copyDataBase();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private boolean checkDataBase()
	{   SQLiteDatabase checkDB = null;		
		try {	checkDB = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.OPEN_READWRITE); }
		catch (Exception e) {System.out.println("������ "+e.getMessage().toString()+", ������� ������");}
		    if (checkDB != null) {checkDB.close();} 
		return checkDB != null;
	}
	
	private void copyDataBase() throws IOException
	{ 	InputStream is = context.getAssets().open(DBName);		
	    OutputStream os = new FileOutputStream(path);
		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = is.read(buffer)) > 0) {os.write(buffer, 0, bytesRead); };
		is.close();
		os.close();
	}

	@Override
	public void close()
	{
		if (db != null) {db.close();}
		super.close();
	}
	
	
	
	
	
	@Override
	public void onCreate(SQLiteDatabase db) {	}
    @Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {	}
	
	

}
