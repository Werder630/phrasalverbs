package information;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.example.phraseverbs.R;

public class MyFragment extends Fragment implements OnClickListener{
	
	int currentpage;
	View sendMail, goToGP;
	 
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		currentpage = this.getArguments().getInt("pos");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle bundle) {
		
		View v = null; 
		
		switch (currentpage)
		{
		case 0: {v = inflater.inflate(R.layout.info_page_test_1, container, false); break;}
		case 1: {v = inflater.inflate(R.layout.info_page_test_2, container, false); break;}
		case 2: 
		   {v = inflater.inflate(R.layout.info_page_test_3, container, false); 
			sendMail = v.findViewById(R.id.info_send_mail);		
			sendMail.setOnClickListener(this);	
			
		    goToGP = v.findViewById(R.id.info_go_to_GP);
			goToGP.setOnClickListener(this);
		   break;}
		}
		
		return v;
	}
	
	static public MyFragment newIstance(int pos)
	{
		MyFragment mf = new MyFragment();
		Bundle b = new Bundle();
		b.putInt("pos", pos);
		mf.setArguments(b);
		
		return mf;
	}

	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
		case R.id.info_send_mail: 
		   {   Intent i = new Intent(Intent.ACTION_SEND);
		      
		       
		       i.putExtra(Intent.EXTRA_EMAIL, new String[] {"werder630@gmail.com"});
		       i.putExtra(Intent.EXTRA_SUBJECT, "� ���������� PhrasalVerbsInUse");
		       i.putExtra(Intent.EXTRA_TEXT, "");
		       i.setType("message/rfc822");
		       
		       startActivity(Intent.createChooser(i, "��������� ������"));
			   break;
		   }
		case R.id.info_go_to_GP:
		   {   Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=Werder"));
		   
		       startActivity(i);
			   break;
		   }
		}
		
		
	}
	
	


}
