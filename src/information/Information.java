package information;

import com.example.phraseverbs.R;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

public class Information extends FragmentActivity {
	ViewPager vp;
	
	static String[] titles = new String[3];

	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint("NewApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		ActionBar ab = this.getActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home)
		{this.finish();}
		return super.onOptionsItemSelected(item);
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_information);
		
		titles[0] = getResources().getString(R.string.info_title_1);
		titles[1] = getResources().getString(R.string.info_title_2);
		titles[2] = getResources().getString(R.string.info_title_3);
		
		vp = (ViewPager)this.findViewById(R.id.info_viewpager);		
		
		vp.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
	}


	public class MyPagerAdapter extends FragmentPagerAdapter  {

		public MyPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int pos) {
			
			return MyFragment.newIstance(pos);
		}

		@Override
		public int getCount() {
			return 3;
		}
		
		@Override
	    public CharSequence getPageTitle(int position) {
	      return titles[position];
	    }
	}


}
