package checking;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import my.engapps.PhraseVerbsInUse.DBHelper;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class QuestionFirst extends Questions {

	List<Integer> isIdAnswerVerbYet = new ArrayList<Integer> ();
	boolean isEnglishQuestion;
	int randomPropos;
	EnglishQuestion eq;
	RussianQuestion rq;
	
	QuestionFirst(Context context, SQLiteDatabase db) 
	{   this.context = context;
	    this.db = db;
		answers = new ArrayList<Answers>();
		eq = new EnglishQuestion();
		rq = new RussianQuestion();
	}
	
	
	@Override 
	public String getQuestion() {		 
		
		isEnglishQuestion = Math.random() < 0.5;

		if (isEnglishQuestion) {return eq.getQ();} 
		else {return rq.getQ();}
	} 
 
	@Override
	public List<Answers> getAnswer() {		 
		if (isEnglishQuestion) {eq.getA();} 
		else {rq.getA();}
		
		return answers;
	}
	
	  //�������� �� ��� �� ����� ������
		int getNotSameQuestion(int count, List<Integer> list)
		{   int result;			
		    do 
		    {result = new Random().nextInt(count);}
		    while (isNumberInList(result, list));
		    
		    list.add(result);	
		    	
	    	return result;
		}
	  
	  
	  
//////////////////////////////////////////////////////////////////////////////////////
	  //���������� ����� ��� ��������� ���������� ������� � ������� �������
/////////////////////////////////////////////////////////////////////////////////////
	  
	  class EnglishQuestion  	  
	  {  
		  Cursor curVerb, curCurrentVerb2Propos, curNotCurrentVerb2Propos;		  
		  
		  String getQ()
		  {//�������� ������ �� ������� ��������
				curVerb = db.rawQuery("Select * from "+DBHelper.V, null);
				//����� ������������ ����� �������	    
			    int randomVerb = new Random().nextInt(curVerb.getCount());
			    //���������� ������ � ����� �������
			    curVerb.moveToPosition(randomVerb);
			    //�������� �������
			    updateInnerJoin(curVerb.getInt(0));	    	    	        
			    //�������� ��������� ������� ��� ����� �������
		        int randomPropos = new Random().nextInt(curCurrentVerb2Propos.getCount());
			    //���������� ������ �������� � ��������� �������� �������
			    curCurrentVerb2Propos.moveToPosition(randomPropos);
			    //�������� �������� �������� �� ������� ���������
			    String proposOrig = getProposById(curCurrentVerb2Propos.getInt(2));

				return curVerb.getString(1)+" "+proposOrig;}
		  
		  void getA()
		  {
			  answers.clear();
				int randomAnswer;
				//��������� �����, ����� �� ������� ����� ����������
				byte randomNumberRightAnswer = (byte)new Random().nextInt(4);
				
				isIdAnswerVerbYet.add(curCurrentVerb2Propos.getPosition());
				
				for (byte i = 0; i <= 3; i++)
				{
					//���� �������� ���������� �����
					if (i == randomNumberRightAnswer) {
						answers.add(new Answers(true, curCurrentVerb2Propos.getString(3)));
					}
					//��� ���������
					else {
						//������� ����������� �����							
						randomAnswer = getNotSameQuestion(curNotCurrentVerb2Propos.getCount(), isIdAnswerVerbYet);
						curNotCurrentVerb2Propos.moveToPosition(randomAnswer);
						answers.add(new Answers(false, curNotCurrentVerb2Propos.getString(3)));
					}
				}
				
				isIdAnswerVerbYet.clear();

		  }
		  
		//���������� ������� �� ������ ���������� ���� ������
		  private void updateInnerJoin(int pos)
		  {  
			  curCurrentVerb2Propos = db.rawQuery("select "
			  		+ "_id, "
			  		+ "id_verb, "
			  		+ "id_proposition, "
			  	    + "translate "
			  		+ "from "+DBHelper.V2P+" where id_verb = ?", new String[] {""+pos});
			  
			  curNotCurrentVerb2Propos = db.rawQuery("select "
				  		+ "_id, "
				  		+ "id_verb, "
				  		+ "id_proposition, "
				  	    + "translate "
				  		+ "from "+DBHelper.V2P+" where id_verb <> ?", new String[] {""+pos});
			  
		  }
		  
		  //�������� ������� �� ������
		  private String getProposById(int pos)
		  {
			  Cursor cursor = db.rawQuery("select "
				  		+ "original "
				  		+ "from "+DBHelper.P+" where _id = ?", new String[] {""+pos});
			  cursor.moveToFirst();
			  return cursor.getString(0);
		  }
		  

		  
	  }
	  
//////////////////////////////////////////////////////////////////////////////////////
     //���������� ����� ��� ��������� �������� ������� � ���������� �������
/////////////////////////////////////////////////////////////////////////////////////	 
	  class RussianQuestion  
	  { 
		  Cursor curRightVerb, curAllVerb, curRightPropos, curAllPropos, curVerb2Propos;
		  List<Integer> isIdProposYet = new ArrayList<Integer> ();	
		  List<Integer> isIdVerbYet = new ArrayList<Integer> ();	
		  
		  String getQ()
		  { //�������� ������ �� ������� ������
				curVerb2Propos = db.rawQuery("Select "
						+ " id_verb, " 
						+ " id_proposition, "
						+ " translate"+" from "+DBHelper.V2P, null);
		    	//����� ������������ ����� �� ���� �������	    
			    int randomV2P = getNotSameQuestion(curVerb2Propos.getCount(), isIdQuestionVerbYet);
			    //���������� ������ � ���� �������
			    curVerb2Propos.moveToPosition(randomV2P);
			    //�������� �������
			    updateInnerJoin(curVerb2Propos.getInt(0), curVerb2Propos.getInt(1));	    	    	        

				return curVerb2Propos.getString(2);
		    	 
		  }
		  
		  void getA()
		  {
				int randomPropos;
				int randomVerb;
				//�������� ��������� ������� ����� �� ��������� ����� ������
				byte randomRightAnswer = (byte)new Random().nextInt(4);
				
				for (byte i = 0; i <= 3; i++)
				{   if (i == randomRightAnswer)
					   {answers.add(new Answers(true, curRightVerb.getString(0)+" "+curRightPropos.getString(0)));   }
				    else 
			     	   {				    	
				    	randomPropos = getNotSameQuestion(curAllPropos.getCount(), isIdProposYet);
				    	randomVerb = getNotSameQuestion(curAllVerb.getCount(), isIdVerbYet);
			           
			           curAllPropos.moveToPosition(randomPropos);
			           curAllVerb.moveToPosition(randomVerb);
				    	
				       answers.add(new Answers(false, curAllVerb.getString(0)+" "+curAllPropos.getString(0)));   }
				}
				
				isIdProposYet.clear();
		  }
		  
			private void updateInnerJoin(int id_v, int id_p) {
				curRightVerb = db.rawQuery("Select original from "+DBHelper.V+
						" where _id = ?", new String[] {""+id_v});
				curRightVerb.moveToFirst();
				
				curRightPropos = db.rawQuery("Select original from "+DBHelper.P+
						" where _id = ?", new String[] {""+id_p});
				curRightPropos.moveToFirst();
				
				curAllVerb = db.rawQuery("Select original from "+DBHelper.V, null);
				
				curAllPropos = db.rawQuery("Select original from "+DBHelper.P, null);	

			}
	  
	  }
	

}
