package checking;

import java.util.List;

import android.content.Context;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.phraseverbs.R;

public class ResultAdapter extends ArrayAdapter<String> {

	View row;
	LayoutInflater inflater;
	Context context;
	List<String> question, given, right;
	List<Boolean> isRight;
	
	public ResultAdapter(Context context, List<String> q, List<String> g, List<String> r, List<Boolean> isR) {
		super(context, R.layout.check_result_right, g);
		this.context = context;
		this.question = q;
		this.given = g;
		this.right = r;
		this.isRight = isR;
		inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
			
		if(isRight.get(position))
		{
			row = inflater.inflate(R.layout.check_result_right, null);
			((TextView)row.findViewById(R.id.check_result_given)).setText(given.get(position));
		}
		else 
		{
			row = inflater.inflate(R.layout.check_result_wrong, null);			
			((TextView)row.findViewById(R.id.check_result_given)).setText(given.get(position));
			((TextView)row.findViewById(R.id.check_result_right)).setText(right.get(position));
		}
		((TextView)row.findViewById(R.id.check_result_question)).setText((position+1)+") "+question.get(position));
		
		return row;
	}
	
	

}
