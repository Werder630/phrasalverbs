package checking;

import java.util.ArrayList;
import java.util.List;

public class AnswerResult  {

	byte rightAnswerQuantity;
	List<String> givenAnswerList, rightAnswerList, questionList;
	List<Boolean> isRightList;
	byte quantityAnswered;
	

	AnswerResult()
	{   rightAnswerQuantity = 0;
		givenAnswerList = new ArrayList<String> ();
		rightAnswerList = new ArrayList<String> ();
		questionList = new ArrayList<String> ();
		isRightList = new ArrayList<Boolean> ();
	}
	
	//����� ��� �������
	public void setQustion(String question)
	{
		questionList.add(question);
	}
	
	//����� ��� ������������� ������
	public void setAnswer(String given, String right)
	{   quantityAnswered++;
		isRightList.add(false);
		givenAnswerList.add(given);
		rightAnswerList.add(right);		
	}
	
	//����� ��� ����������� ������
	public void setAnswer(String given)
	{       quantityAnswered++;
			rightAnswerQuantity++;
			isRightList.add(true);
			givenAnswerList.add(given);
			rightAnswerList.add(given);
	}
		
	List<String> getGivenAnswer()
	{
	   return givenAnswerList;	
	}
	
	List<String> getRightAnswer()
	{
	   return rightAnswerList;	
	}
	
	List<Boolean> getIsRightAnswer()
	{
		return isRightList;	
	}
	
	List<String> getQuestionList()
	{
		return questionList;
	}
	
	byte getRightAnsweredQuantity()
	{ 
		return rightAnswerQuantity;
	}

}
