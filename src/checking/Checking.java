package checking;

import java.util.List;

import my.engapps.PhraseVerbsInUse.DBHelper;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.phraseverbs.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;


public class Checking extends Activity implements OnClickListener {

	TextView question_tv;
	Button[] answer_buttons;
	ProgressBar pb;
	SQLiteDatabase db;
    Button confirm_answer_b;
    byte difficult_level;
    Questions qi;
    AlertDialog ad;
    byte clickPositionAnswer;
    byte rightPositionAnswer;
    String rightStringAnswer;
    static byte currentQuestion;
    static AnswerResult ar;
    List<Answers> answers;    
    Drawable button_press, button_unpress;
    MenuItem cq;
    private InterstitialAd interstitial;
    

 ////////////////////////////////////////////////////////////////////////////////////////
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_checking);
				
		question_tv = (TextView)this.findViewById(R.id.check_question_tv);
		
		button_press = this.getResources().getDrawable(
				R.drawable.check_variety_press);
		button_unpress = this.getResources().getDrawable(
				R.drawable.check_variety_unpress);
		
		answer_buttons = new Button[]
				{(Button)this.findViewById(R.id.check_answer_1_b),
			 	 (Button)this.findViewById(R.id.check_answer_2_b),
			 	 (Button)this.findViewById(R.id.check_answer_3_b),
				 (Button)this.findViewById(R.id.check_answer_4_b)};
		for (int i = 0; i <= 3; i++)
		{answer_buttons[i].setOnClickListener(this);}
		
		confirm_answer_b = (Button)this.findViewById(R.id.check_answer_b);
		confirm_answer_b.setOnClickListener(this);
		confirm_answer_b.setClickable(false);
		confirm_answer_b.setTextColor(Color.GRAY);
		
		clickPositionAnswer = 0;
		rightPositionAnswer = 0;
		currentQuestion = 0;
		
		pb = (ProgressBar)this.findViewById(R.id.progressBar);
		pb.setMax(9);
		ar = new AnswerResult();
		
		//���� ��� ������������� �������////////////////////
		{ interstitial = new InterstitialAd(this);
	    interstitial.setAdUnitId("ca-app-pub-7111132956833864/6226015831");
	    AdRequest adRequest = new AdRequest.Builder().build();
	    interstitial.loadAd(adRequest); }
		/////////////////////////////////////////////////////
		
		getSelectLevelDifficultDialog();
	}

	////////////////////////////////////////////////////////////////////////////////////////	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint("NewApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		ActionBar ab = this.getActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		
		MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.checking, menu);
		
	    cq = (MenuItem)menu.findItem(R.id.currentQurstionMenu);
	    
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home)
		{quitDialog();}
		return super.onOptionsItemSelected(item);
	}
	
	//����������� ������� ��� ������� ��������� ����
	private void quitDialog() {
		
		AlertDialog.Builder adb = new AlertDialog.Builder(this);
		adb.setTitle("����� �� �����");
		adb.setMessage("��� �������� ������� ���� ����� �������. �� ������������� ������ ����� � ������� ����?");
		adb.setPositiveButton("��", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				quit();
			}		
		});
	
         adb.setNegativeButton("���", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				dialog.cancel();
			}
		});
         
         AlertDialog ad = adb.create();
         ad.show();
		
	}
	
	
	@Override
	public void onBackPressed() {
		 quitDialog();
	}

	private void quit()
	{this.finish();}
	
///////////////////////////////////////////////////////////////////////////////////////
			

	private void getSelectLevelDifficultDialog() {
		LayoutInflater li = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = li.inflate(R.layout.check_level_difficult_dialog, null);
		
		View easy = v.findViewById(R.id.check_button_level_easy);
		easy.setOnClickListener(this);
		View medium = v.findViewById(R.id.check_button_level_medium);
		medium.setOnClickListener(this);
			
		AlertDialog.Builder adb = new AlertDialog.Builder(this);
		adb.setTitle("������� ���������");
		adb.setView(v);
		
		ad = adb.create();
		ad.setOnCancelListener(new DialogInterface.OnCancelListener() {
			
			@Override
			public void onCancel(DialogInterface dialog) {
			   if (difficult_level == 0) 
			     {difficult_level = 1;
			     setTitle("����: ������");
			     getDate();}
			}
		});
	
		ad.show();
		 
	}

///////////////////////////////////////////////////////////////////////////////////////

	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
		//��������� ������� ����������� ����/////////////////////////
		case R.id.check_button_level_easy:
		{   difficult_level = 1;
		    setTitle("����: ������");
		    ad.cancel();
		    getDate();
		    break;
		}
		
		case R.id.check_button_level_medium:
		{   difficult_level = 2;
	    	setTitle("����: �������");
		    ad.cancel();
		    getDate();
			break;
		}
		
		
		//��������� ������� �� �������� �������//////////////////////
		case R.id.check_answer_1_b:
		{   clickButtonAnswer((byte) 1);
			break;
		}

		case R.id.check_answer_2_b:
		{   clickButtonAnswer((byte) 2);
			break;
		}
		
		case R.id.check_answer_3_b:
		{   clickButtonAnswer((byte) 3);
			break;
		}
		
		case R.id.check_answer_4_b:
		{   clickButtonAnswer((byte) 4);
			break;
		}
		
		//������� �� ������ ������///////////////////////////////////
		
		case R.id.check_answer_b:
		 
		ar.setQustion(question_tv.getText().toString());
	    if (clickPositionAnswer == rightPositionAnswer)
		    {ar.setAnswer(rightStringAnswer);}
	    else {ar.setAnswer(answers.get(clickPositionAnswer-1).getAnswer(), rightStringAnswer);}
			   
		{if (currentQuestion == 10) 
		       {
			 if (interstitial.isLoaded()) {
			      interstitial.show();
			    } else {System.out.println("������� "+interstitial.isLoaded());}
			  Intent intent = new Intent(this, CheckingResult.class);
		       startActivity(intent);
		       finish();
		       }
		   else
			   {
			   getDate();
			   pb.setProgress(currentQuestion-1);
			   break;
		   }
		  }
		}
		
	}
	
	//��������� ������� ����� ������� �� ������ ������
	private void clickButtonAnswer(byte ans) {
		this.clickPositionAnswer = ans;
		for (int i = 0; i <= 3; i++)
		{
			setFrameForAnswerButton(answer_buttons[i], false);
		}
		setFrameForAnswerButton(answer_buttons[ans-1], true);
		confirm_answer_b.setClickable(true);
		confirm_answer_b.setTextColor(Color.BLACK);
	}
	
	

	//���������� ����� ������ ������� �� ����
	private void getDate() {	
		db = new DBHelper(this).openDataBase();
		
		currentQuestion++;
		//��������� ����� �������� ������� � ������ ��� � ActionBar 
		if (cq != null)
		{cq.setTitle(""+ currentQuestion +" / 10");}
		
		//������ ����� �� ������ ���� ��������� ������
		if(currentQuestion == 10) {this.confirm_answer_b.setText("���������");}
		//���������� ��� ��� ������
		for (int i = 0; i <= 3; i++)
		{
			setFrameForAnswerButton(answer_buttons[i], false);
		}
		//������ ���������� ������ ������
		confirm_answer_b.setClickable(false);
		confirm_answer_b.setTextColor(Color.GRAY);
		
		//�������� ������ � ����������� �� ���������
		switch (this.difficult_level)
		{ 
		case 1:  
		   {   qi = new QuestionFirst(this, db);   
			   break;}
		   
		case 2:
		   {   qi = new QuestionSecond(this, db);
			   break;}
	  		
		}
			
		//�������� ���� �� ����� ����������� �������
		question_tv.setText(qi.getQuestion());
		answers = qi.getAnswer();
		
		for (int i = 0; i <= 3; i++)
		{
			answer_buttons[i].setText(answers.get(i).getAnswer());
			if (answers.get(i).isRight()) 
			     {rightPositionAnswer = (byte)(i+1);
			     rightStringAnswer = answers.get(i).getAnswer();
			     }
	    }
		
		db.close();
	}
	
	//����� ��� ����������� ������ �������
	@SuppressWarnings("deprecation")
	private void setFrameForAnswerButton(Button b, boolean isPress)
	{
		if (!isPress)
		  {
			if (Build.VERSION.SDK_INT >= 16 )
			{b.setBackground(button_unpress); }
			else {b.setBackgroundDrawable(button_unpress);}
		  }
		
		else 
		  {
			if (Build.VERSION.SDK_INT >= 16 )
			{b.setBackground(button_press); }
			else {b.setBackgroundDrawable(button_press);}
		  }
		
			
	
	}

}
