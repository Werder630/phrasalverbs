package checking;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.phraseverbs.R;

public class CheckingResult extends Activity implements View.OnClickListener{

	View result, again, finish;
	ListView lv;
	AnswerResult ar;
	TextView t;
	
	
	public CheckingResult() {
		this.ar = Checking.ar;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_checking_result);
		
		again = this.findViewById(R.id.checkres_but_again);
		again.setOnClickListener(this);
		finish = this.findViewById(R.id.checkres_but_finish);
		finish.setOnClickListener(this);
		lv = (ListView)this.findViewById(R.id.checkres_lv);
		lv.setAdapter(new ResultAdapter(this, ar.getQuestionList(), ar.getGivenAnswer(), ar.getRightAnswer(), ar.getIsRightAnswer())); 
		((TextView)findViewById(R.id.checkres_tv)).setText("Правильных ответов: "+ar.getRightAnsweredQuantity());
	}

	@Override
	public void onClick(View v) {
		
		switch (v.getId())
		{
		case R.id.checkres_but_again:
		   {   this.startActivity(new Intent(this, Checking.class));
			   break;
		   }
		   
		case R.id.checkres_but_finish:
		   {   			   
			   break;
		   }
		}
		finish();
	}



}
