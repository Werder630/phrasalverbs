package checking;

public class Answers {

	boolean right;
	String answer;
	
	Answers(boolean isright, String ans)
	{   this.right = isright;
		this.answer = ans;	}
	
	boolean isRight()
	{return right;}
	
	String getAnswer()
	{return answer;}
	
	
}
