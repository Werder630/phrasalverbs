package checking;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import my.engapps.PhraseVerbsInUse.DBHelper;
import checking.QuestionFirst.EnglishQuestion;
import checking.QuestionFirst.RussianQuestion;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class QuestionSecond extends Questions {

	
	List<Integer> isIdAnswerVerbYet = new ArrayList<Integer> ();
	boolean isEnglishQuestion;
	int randomPropos;
	EnglishQuestion eq;
	RussianQuestion rq;
	
	QuestionSecond(Context context, SQLiteDatabase db) 
	{   this.context = context;
	    this.db = db;
		answers = new ArrayList<Answers>();
		eq = new EnglishQuestion();
		rq = new RussianQuestion();
	}
	
	
	@Override 
	public String getQuestion() {		 
		
		isEnglishQuestion = Math.random() < 0.5;
		if (isEnglishQuestion) {return eq.getQ();} 
		else {return rq.getQ();}
	} 
 
	@Override
	public List<Answers> getAnswer() {		 
		if (isEnglishQuestion) {eq.getA();} 
		else {rq.getA();}
		
		return answers;
	}
	
	  //�������� �� ��� �� ����� ������
		int getNotSameQuestion(int count, List<Integer> list)
		{   int result;			
		    do 
		    {result = new Random().nextInt(count);}
		    while (isNumberInList(result, list));
		    
		    list.add(result);	
		    	
	    	return result;
		}
	  
	  
	  
//////////////////////////////////////////////////////////////////////////////////////
	  //���������� ����� ��� ��������� ����������� ������� � ������� �������
/////////////////////////////////////////////////////////////////////////////////////
	  
	  class EnglishQuestion  	  
	  {  
		  Cursor curAllV2P, curAllV2PRight, curAllV2PWrong, curVerb, CurPropos;
		  List<Integer> IDVerb2Propos = new ArrayList<Integer> ();
		  
		  String getQ()
		  {
                //������� ������ �� ������� ������, � ������� ������ ����� 4 ��� ����� ���������
		    	Cursor temp = db.rawQuery("select id_verb from "+DBHelper.V2P
		    	+" group by id_verb having count(*) >= 4", null);
		    	
		    	//������� ��������� ������
		    	int randomVerb = new Random().nextInt(temp.getCount());
		    	
		    	//���������� ������ �� ��� �������
		    	temp.moveToPosition(randomVerb);
		    	//������� ������� ��� ���� ������
		    	updateInnerJoin(temp.getInt(0));
		    	//������� �� ��������� ���� ������ ������+�������
		    	int randomQuestion = new Random().nextInt(curAllV2P.getCount());
		    	//���������� ������ � ������� � ������� ������ 
		    	curAllV2P.moveToPosition(randomQuestion);
		    	//������� ������ ��������, ����� ������� ������ ��������
		    	CurPropos = db.rawQuery("select original from "+DBHelper.P
		    			+" where _id = ?", new String[] {""+curAllV2P.getInt(2)}  );
		    	CurPropos.moveToFirst();
		    	
		    	getV2PRightAndWrong(curAllV2P.getInt(0), temp.getInt(0));
		    	
		    	
				return curVerb.getString(0)+" "+CurPropos.getString(0);}
		  
		  void getA()
		  {			  
			  byte randomNumberAnswer = (byte)new Random().nextInt(4);
			  
			  for (byte i = 0; i <= 3; i++)
				{  if (i == randomNumberAnswer) 
				      {answers.add(new Answers(true, ""+curAllV2PRight.getString(0)));}
				   else
				      {
					   int randomtemp = getNotSameQuestion(curAllV2PWrong.getCount(), IDVerb2Propos);
					   curAllV2PWrong.moveToPosition(randomtemp);
					   answers.add(new Answers(false, ""+curAllV2PWrong.getString(0)));
				      }
			       
			    }
			  
			  IDVerb2Propos.clear();
		  }
		  
		  private void updateInnerJoin(int pos)
		  {  
			  curAllV2P = db.rawQuery("select "
			  		+ "_id, "
			  		+ "id_verb, "
			  		+ "id_proposition, "
			  	    + "translate "
			  		+ "from "+DBHelper.V2P+" where id_verb = ?", new String[] {""+pos});
			  		  
			  curAllV2P.moveToFirst();
			  
			  curVerb = db.rawQuery("select original from "+DBHelper.V+" where _id = ?", new String[] {""+pos});
			  
			  curVerb.moveToFirst();  
		  }
		  
		  private void getV2PRightAndWrong(int pos, int verb)
		  {
			  curAllV2PRight = db.rawQuery("select "
				  	    + "translate "
				  		+ "from "+DBHelper.V2P+" where _id = ?", new String[] {""+pos});
			  curAllV2PRight.moveToFirst();
			  
			  curAllV2PWrong = db.rawQuery("select "
				  	    + "translate "
				  		+ "from "+DBHelper.V2P+" where _id <> ? and id_verb = ?", new String[] {""+pos, ""+verb});
			  curAllV2PWrong.moveToFirst();
		  }

		  
	  }
	  
//////////////////////////////////////////////////////////////////////////////////////
     //���������� ����� ��� ��������� �������� ������� � ���������� �������
/////////////////////////////////////////////////////////////////////////////////////	 
	  class RussianQuestion  
	  { 
		  Cursor curRightVerb, curRightPropos, curAllPropos, curVerb2Propos;
		  List<Integer> isIdProposYet = new ArrayList<Integer> ();	
		  List<Integer> isIdVerbYet = new ArrayList<Integer> ();	
		  
		  String getQ()
		  { //�������� ������ �� ������� ������
				curVerb2Propos = db.rawQuery("Select "
						+ " id_verb, " 
						+ " id_proposition, "
						+ " translate"+" from "+DBHelper.V2P, null);
		    	//����� ������������ ����� �� ���� �������	    
			    int randomV2P = getNotSameQuestion(curVerb2Propos.getCount(), isIdQuestionVerbYet);
			    //���������� ������ � ���� �������
			    curVerb2Propos.moveToPosition(randomV2P);
			    //�������� �������
			    updateInnerJoin(curVerb2Propos.getInt(0), curVerb2Propos.getInt(1));	    	    	        

				return curVerb2Propos.getString(2);
		    	 
		  }
		  
		  void getA()
		  {
				int randomPropos;
				int randomVerb;
				//�������� ��������� ������� ����� �� ��������� ����� ������
				byte randomRightAnswer = (byte)new Random().nextInt(4);
				
				for (byte i = 0; i <= 3; i++)
				{   if (i == randomRightAnswer)
					   {answers.add(new Answers(true, curRightVerb.getString(0)+" "+curRightPropos.getString(0)));   }
				    else 
			     	   {				    	
				    	randomPropos = getNotSameQuestion(curAllPropos.getCount(), isIdProposYet);
			           
			           curAllPropos.moveToPosition(randomPropos);
			           
				       answers.add(new Answers(false, curRightVerb.getString(0)+" "+curAllPropos.getString(0)));   }
				}
				
				isIdProposYet.clear();
		  }
		  
			private void updateInnerJoin(int id_v, int id_p) {
				curRightVerb = db.rawQuery("Select original from "+DBHelper.V+
						" where _id = ?", new String[] {""+id_v});
				curRightVerb.moveToFirst();
				
				curRightPropos = db.rawQuery("Select original from "+DBHelper.P+
						" where _id = ?", new String[] {""+id_p});
				curRightPropos.moveToFirst();

				curAllPropos = db.rawQuery("Select original from "+DBHelper.P, null);	

			}
	  
	  }
	
	  
}
