package studying;

import java.util.ArrayList;
import java.util.List;

import my.engapps.PhraseVerbsInUse.DBHelper;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.phraseverbs.R;

public class Studying extends Activity implements OnClickListener , OnTouchListener{

	TextView verb_orig_tv, propos_tv, current_number_tv, answer_tv, example_orig_tv, example_trans_tv, source_tv;
	Cursor curVerb2Propos, curPropos;
	SQLiteDatabase db;
	Animation anim_start, anim_stop;
	SharedPreferences mSettings;
	float startX = 0, stopX = 0;
	  
	int w, h; 
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_studying); 

		this.findViewById(R.id.stud_rl).setOnTouchListener(this);
		
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);	
		w = metrics.widthPixels;
		h = metrics.heightPixels;
		
		verb_orig_tv = (TextView)this.findViewById(R.id.stud_verb_orig_tv);
		
		propos_tv = (TextView)this.findViewById(R.id.stud_propos_tv);
		propos_tv.setOnClickListener(this);
		
		current_number_tv = (TextView)this.findViewById(R.id.stud_current_number_tv);
		
		answer_tv = (TextView)this.findViewById(R.id.stud_answer_tv);
		
		example_orig_tv = (TextView)this.findViewById(R.id.stud_example_orig_tv);
		
		example_trans_tv = (TextView)this.findViewById(R.id.stud_example_trans_tv);
		
		source_tv = (TextView)this.findViewById(R.id.stud_source_tv);
		
		mSettings = getSharedPreferences("settings", MODE_PRIVATE);  
		
		if (!mSettings.getBoolean("isShowedExplainDialog", false))
		{getExplainDialog();}

		getFirstData(mSettings.getInt("posPropos", 0), mSettings.getInt("posVerb", 0));

	} 
 
 
	private void getExplainDialog() {
		AlertDialog.Builder adb = new AlertDialog.Builder(this);
		adb.setTitle("�������");
		LayoutInflater inflater = getLayoutInflater();
		View view = inflater.inflate(R.layout.stud_explain_dialog, null);
		adb.setView(view);
		
		adb.setPositiveButton("��", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				Editor e = mSettings.edit();
				e.putBoolean("isShowedExplainDialog", true);
		        e.commit();
				dialog.cancel();
				
			}});
		
		AlertDialog ad = adb.create();
		ad.show();
		
	}


	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint("NewApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		ActionBar ab = this.getActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home)
		{this.finish();}
		return super.onOptionsItemSelected(item);
	}
	
	
	@Override
	protected void onPause() {
		super.onPause();
		
		Editor ed = mSettings.edit();
		ed.putInt("posPropos", this.curPropos.getPosition());
		ed.putInt("posVerb", this.curVerb2Propos.getPosition());
        ed.apply();
        
	}

	private void getFirstData(int propos, int verb2propos) {
		db = new DBHelper(this).openDataBase();
		//�������� ��� ������ �� ������� ��������� � ������
		this.curPropos = db.query(DBHelper.P, null, null, null, null, null, null);		
		curPropos.moveToPosition(propos);
		propos_tv.setText(curPropos.getString(1));
		updateInnerJoin(curPropos.getInt(0)); 
		curVerb2Propos.moveToPosition(verb2propos);
		reshreshData();
		db.close();
	}


//////////////////////////////////////////////////////////////////////////////////////
	//��������� ������
/////////////////////////////////////////////////////////////////////////////////////
	
	@Override
	public void onClick(View v) {
		
	switch(v.getId())
    {   
	 //������ ��������� 
	 case R.id.stud_propos_tv:
	 {   db = new DBHelper(this).getReadableDatabase();
	
	     final int tempPos = curPropos.getPosition();
	 
		 List<String> al = new ArrayList<String>();
		 curPropos.moveToFirst();
		 
		 //������������ ������� ��������� �� �������
		 while(!curPropos.isAfterLast())
		 {   al.add(curPropos.getString(1));
			 curPropos.moveToNext();
		 }
		 
		 //������� �������
		 ArrayAdapter<String> aa = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, al);
		 
		 AlertDialog.Builder adb = new AlertDialog.Builder(this);
		 adb.setTitle("����� ��������");
		 
		 adb.setAdapter(aa, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				//����������� ������ � ������� �������
				curPropos.moveToPosition(which);
				//��������� ��������� ������� �� ���� �������
				updateInnerJoin(which+1);
				//�������� ������ ��� ������ �������
				curVerb2Propos.moveToFirst();
				reshreshData();
				db.close();
			}
		});
		 AlertDialog ad = adb.create();
		 ad.show();
		 
		 ad.setOnCancelListener(new DialogInterface.OnCancelListener() {
			
			@Override
			public void onCancel(DialogInterface dialog) {
				curPropos.moveToPosition(tempPos);
			}
		});
		 
		 
		 break;
	 }
	  }
	}
	
/////////////////////////////////////////////////////////////////////////////////////

	private void getData(boolean isNext) {
		db = new DBHelper(this).openDataBase();
		//���������� ������ ���������� ������� � ����������� �� ��������
		if (isNext) {
			getNextVerb2Propos();
			startAnimation(R.anim.stud_anim_start_right);		
		}
		else {
			getPrevoisVerb2Propos();
			startAnimation(R.anim.stud_anim_start_left);	
		}		
		db.close();
	}
	
	//�������� �����----------------------------------------------------------------
		private void startAnimation(final int resAnim) {
			Animation anim = AnimationUtils.loadAnimation(this, resAnim);
	    	verb_orig_tv.startAnimation(anim);
	    	if (resAnim == R.anim.stud_anim_start_right) 
	    	   {if (curVerb2Propos.isFirst()) {propos_tv.startAnimation(anim);}}
	    	else { if (curVerb2Propos.isLast()) {propos_tv.startAnimation(anim);}}
	    	
	    	answer_tv.startAnimation(anim);
	    	example_orig_tv.startAnimation(anim);
	    	example_trans_tv.startAnimation(anim);
	    	source_tv.startAnimation(anim);
	    	anim.setAnimationListener(new Animation.AnimationListener() {
				
				@Override
				public void onAnimationStart(Animation animation) {	}				
				@Override
				public void onAnimationRepeat(Animation animation) { }
				
				@Override
				public void onAnimationEnd(Animation animation) {
					reshreshData();	
					if (resAnim == R.anim.stud_anim_start_right)
					{stopAnimation(R.anim.stud_anim_stop_right);}
					else 
					{stopAnimation(R.anim.stud_anim_stop_left);}
					
				}
			});

		}
		
		//�������� �����----------------------------------------------------------------
		private void stopAnimation(int resAnim) {
			Animation anim = AnimationUtils.loadAnimation(this, resAnim);
	    	verb_orig_tv.startAnimation(anim);
	    	
	    	if (resAnim == R.anim.stud_anim_stop_right) 
	    	   {if (curVerb2Propos.isFirst()) {propos_tv.startAnimation(anim);}}
	    	else { if (curVerb2Propos.isLast()) {propos_tv.startAnimation(anim);}}
	    	
	    	answer_tv.startAnimation(anim);
	    	example_orig_tv.startAnimation(anim);
	    	example_trans_tv.startAnimation(anim);
	    	source_tv.startAnimation(anim);

		}

  //�������� ��������� ������--------------------------------------------------------------
	void reshreshData() {		
		verb_orig_tv.setText(curVerb2Propos.getString(0));
		propos_tv.setText(curPropos.getString(1));
		current_number_tv.setText(""+(curVerb2Propos.getPosition()+1)+" �� "+curVerb2Propos.getCount());
		answer_tv.setText(curVerb2Propos.getString(1));
		example_orig_tv.setText("   "+curVerb2Propos.getString(2));
		example_trans_tv.setText("   "+curVerb2Propos.getString(3));
		source_tv.setText(curVerb2Propos.getString(4));				
	}
	
 //���������� ������� INNER JOIN ������
  private void getNextVerb2Propos() {	  		 
	  //���� ��������� ������ ���������, �� ���� �������� ������ ���������
	  if (curVerb2Propos.isLast())
	  {   //���� ������ �������� ��������� - �������� � ������
		  if (curPropos.isLast()) {curPropos.moveToFirst();}
		    //����� �������� ����� �� ������� ���������
		    else {curPropos.moveToNext();}
	        updateInnerJoin(curPropos.getInt(0)); 
		     curVerb2Propos.moveToFirst(); 
	  } 
	  //����� �������� ����� �� ���������� ������� 
	  else {curVerb2Propos.moveToNext();}
  }
  
  //���������� ������� INNER JOIN �����
  private void getPrevoisVerb2Propos() {
	  //���� ��������� ������ ������, �� ���� �������� ������ ���������
	  if (curVerb2Propos.isFirst()) 
	  { //���� ������ �������� ������ - �������� � �����
		  if (curPropos.isFirst()) {curPropos.moveToLast();}
		  //����� �������� ���� �� ������� ���������
		  else {curPropos.moveToPrevious();}
		  updateInnerJoin(curPropos.getInt(0));
		  curVerb2Propos.moveToLast();
		  }
	 
	  else {curVerb2Propos.moveToPrevious();}
	  
	}

	//���������� ������� �� ������ ���������� ���� ������
  private void updateInnerJoin(int pos)
  {  
	  curVerb2Propos = db.rawQuery("select " +
	    		"v.ORIGINAL, " +           //0     
	    		"v2p.translate, " +        //1
	    		"v2p.example_orig, " +     //2   
	    		"v2p.example_trans , " +   //3
	    		"v2p.sour�e " +           //4
	    		"from "+DBHelper.V+" as v inner join "+DBHelper.V2P+" as v2p on v._id = v2p.id_verb where v2p.id_proposition = ?", new String[] {""+pos});
	  
  }
	
	//��������� ������
	@Override
	public boolean onTouch(View v, MotionEvent event) {
			
		switch (event.getAction())
		{
		case MotionEvent.ACTION_DOWN:
		   {
			   startX = event.getX();
			   break;
		   }
		case MotionEvent.ACTION_UP:
		   {
			   stopX = event.getX();
			   //�������� ������ ������ �����
			   if (startX > stopX+10) {getData(true); }
			   //�������� ������ ����� ������
			   else if (startX < stopX-10) {getData(false);}			    
			   break;
		   }
		   
		}
		
		return true;
	}

	

}
	
	
	




